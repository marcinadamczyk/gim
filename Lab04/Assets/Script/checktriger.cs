﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checktriger : MonoBehaviour
{
    private Renderer _renderer;

    void Start()
    {
        _renderer = GetComponent<Renderer>();
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            PlaySound(collider);
        }
    }

    private void PlaySound(Collider collider)
    {
        var sound = collider.GetComponent<AudioSource>();
        sound.Play();
    }
}

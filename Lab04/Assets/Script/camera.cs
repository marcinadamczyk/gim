﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera : MonoBehaviour
{

    public Vector3 offset;
    public float rotationSpeed;
    float mouseX, mouseY;
    public Transform Player, Target;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mouseX += Input.GetAxis("Mouse X") * rotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * rotationSpeed;
        Player.transform.rotation = Quaternion.Euler(mouseY, mouseX, 0);

    }
}

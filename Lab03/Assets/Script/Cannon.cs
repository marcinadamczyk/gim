﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cannon : MonoBehaviour
{
    public GameObject CannonBall;
    Rigidbody CannonBallRB;
    public Transform ShootPoint;
    public float force;
    private bool firebool = true;
    void Start()
    {
        force = 3000;
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            CannonBall.GetComponent<ConstantForce>().force = transform.forward * force;
            var cannonBall2 = Instantiate(CannonBall, ShootPoint.position, ShootPoint.rotation);
            Destroy(cannonBall2, 5f);
        }
    }
}

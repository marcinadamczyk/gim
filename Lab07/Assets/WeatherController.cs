﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherController : MonoBehaviour
{
    public GameObject snow;
    public GameObject rain;
    void Start()
    {
        rain.SetActive(false);
        snow.SetActive(false);
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            rain.SetActive(false);
            snow.SetActive(true);
        }

        if (Input.GetButtonDown("Fire2"))
        {
            snow.SetActive(false);
            rain.SetActive(true);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimplePlatformController : MonoBehaviour
{
    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;

    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck;

    private bool _grounded = false;
    private Animator _anim;
    private Rigidbody2D _rb2d;

    public Text countText;
    public Text winText;
    public Text timeText;

    private int _count;
    private float time;

    private void Start()
    {
        _count = 0;

        winText.gameObject.SetActive(false);
        timeText.text = "";
    }

    private void Awake()
    {
        _anim = GetComponent<Animator>();
        _rb2d = GetComponent<Rigidbody2D>();

        Timer();
    }

    private void Update()
    {
        Timer();

        _grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        if(Input.GetButtonDown("Jump") && _grounded)
        {
            jump = true;
        }
    }

    void Timer()
    {
        time = time + Time.deltaTime;

        if (time <= 10)
        {
            timeText.text = $"Czas: {10 - Mathf.RoundToInt(time)}";
        }

        if(time>10)
        {
            winText.gameObject.SetActive(true);
        }
    }

    private void FixedUpdate()
    {
        var h = Input.GetAxis("Horizontal");
        _anim.SetFloat("Speed", Mathf.Abs(h));

        if(h*_rb2d.velocity.x<maxSpeed)
        {
            _rb2d.AddForce(Vector2.right * h * moveForce);
        }

        if(Mathf.Abs(_rb2d.velocity.x)>maxSpeed)
        {
            _rb2d.velocity = new Vector2(Mathf.Sign(_rb2d.velocity.x) * maxSpeed, _rb2d.velocity.y);
        }

        if(h>0 && !facingRight)
        {
            Flip();
        }
        else if(h<0&&facingRight)
        {
            Flip();
        }

        if(jump)
        {
            _anim.SetTrigger("Jump");
            _rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        var tempScale = transform.localScale;
        tempScale.x *= -1;
        transform.localScale = tempScale;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("SilverCoin"))
        {
            Debug.Log(collision.gameObject.tag);
            collision.gameObject.SetActive(false);
            Destroy(collision.gameObject);
            _count++;
            countText.text = $"{_count}";
            Debug.Log($"{_count}");
        }
        if (collision.gameObject.CompareTag("GoldCoin"))
        {
            Debug.Log(collision.gameObject.tag);
            collision.gameObject.SetActive(false);
            Destroy(collision.gameObject);
            _count+=4;
            countText.text = $"{_count}";
        }

    }
}

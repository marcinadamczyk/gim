﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlatform : MonoBehaviour
{
    public int maxPlatforms = 20;
    public GameObject platform;
    public float horizontalMin = 7.5f;
    public float horizontalMax = 14f;
    public float verticalMin = -6f;
    public float verticalMax = 6f;

    private Vector2 _originPosition;

    private void Start()
    {
        _originPosition = transform.position;
        Spawn();
    }

    private void Spawn()
    {
        for (int i = 0; i < maxPlatforms; i++)
        {
            var randomPosition = _originPosition + new Vector2(Random.Range(horizontalMin, horizontalMax), Random.Range(verticalMin, verticalMax));
            Instantiate(platform, randomPosition, Quaternion.identity);
            _originPosition = randomPosition;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DestroyScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            Application.LoadLevel(Application.loadedLevel);
        }
    }

    private void OnCollision2D(GameObject collision)
    {
        if(collision.gameObject.CompareTag("Platform"))
        {
            Destroy(gameObject);
        }
    }
}

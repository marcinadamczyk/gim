﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triger : MonoBehaviour
{
    public GameObject Door;
    public Animator Anim;

    void Start()
    {

    }

    void Update()
    {
        Anim = Door.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            Anim.SetTrigger("Open");
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")
        {
            Anim.SetTrigger("Close");
        }
    }
}
